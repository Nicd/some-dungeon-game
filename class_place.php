<?php

class Place
{
   private $level;
   private $coord;

   public function __construct(Level $level, Coord $coord)
   {
      $this->level = $level;
      $this->coord = $coord;
   }

   public function getCoord()
   {
      return $this->coord;
   }

   public function getLevel()
   {
      return $this->level;
   }
}
