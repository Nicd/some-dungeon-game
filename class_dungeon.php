<?php

class Dungeon
{
   private $levels;
   private $lowest;
   private $highest;

   public function __construct()
   {
      $this->lowest = 0;
      $this->highest = 0;
      $this->levels = array();
   }

   public function addLevelAbove(Level $level)
   {
      if (!empty($this->levels))
      {
         ++$this->highest;
      }
      $this->levels[$this->highest] = $level;
   }

   public function addLevelBelow(Level $level)
   {
      if (empty($this->levels))
      {
         --$this->lowest;
      }
      $this->levels[$this->lowest] = $level;
   }

   public function getLevels()
   {
      return $this->levels;
   }

   public function getLevel($number)
   {
      return (isset($this->levels[$number]))? $this->levels[$number] : null;
   }

   public function getLevelNumber(Level $level)
   {
      foreach ($levels as $number => $savedLevel)
         if ($savedLevel == $level)
            return $number;
   }
}
