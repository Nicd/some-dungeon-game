<?php

class LevelDrawer
{
   private $disp;
   private $dude;

   public function __construct(Display $disp)
   {
      $this->dude = DudeStore::getDude();
      $this->changeDisplay($disp);
   }

   public function changeDisplay(Display $disp)
   {
      $this->disp = $disp;
   }

   public function drawEverything()
   {
      $this->dude->getPlace()->getLevel()->printLevel();
   }

   public function drawDudeView()
   {
      $this->drawView($this->dude->getBase());
   }

   public function drawView(AbstractBeing $being)
   {
      $radius = $being->getSight();
      $beingX = $being->getPlace()->getCoord()->x;
      $beingY = $being->getPlace()->getCoord()->y;

      // Only print the characters inside the being's sight radius
      for ($x = $beingX - $radius; $x <= $beingX + $radius; ++$x)
      {
         for ($y = $beingY - $radius; $y <= $beingY + $radius; ++$y)
         {
            $obj = $being->getPlace()->getLevel()->whatsAt(new Coord($x, $y));
            if ($obj !== null)
            {
               // Draw path to object and only display those that can be seen
               if ($being->canSee($obj))
                  $this->disp->writeTo($x + 1, $y + 1, $obj);
            }
         }
      }
   }
}
