<?php

class CannotEquipException extends SDGException
{
   public function getMessage()
   {
      return 'Cannot equip item: ' . parent::getMessage();
   }
}
