<?php

interface Item
{
   private $name;
   private $value;

   public function equip(Being wearer);
   public function unequip(Being wearer);
   public function sell(Being seller, Being buyer);
   public function use(Being user);

   public function getName();
   public function getValue();
}
