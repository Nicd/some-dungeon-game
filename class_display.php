<?php

class Display
{
   const STYLE_NONE = 0;
   const STYLE_BOLD = 1;
   const STYLE_ITALIC = 2;
   const STYLE_UNDERLINE = 4;
   const STYLE_BLINK = 8;
   const STYLE_INVERSE = 16;

   const COLOR_NONE = 9;
   const COLOR_BLACK = 0;
   const COLOR_RED = 1;
   const COLOR_GREEN = 2;
   const COLOR_YELLOW = 3;
   const COLOR_BLUE = 4;
   const COLOR_MAGENTA = 5;
   const COLOR_CYAN = 6;
   const COLOR_WHITE = 7;

   private $xSize;
   private $ySize;
   private $stream;
   
   public function __construct($xSize, $ySize, $stream)
   {
      $this->xSize = $xSize;
      $this->ySize = $ySize;
      $this->stream = $stream;
      $this->color = self::COLOR_NONE;
   }

   public function write($text, $style = self::STYLE_NONE, $color = self::COLOR_NONE)
   {
      if ($style != self::STYLE_NONE)
         $this->setStyle($style);
      if ($color != self::COLOR_NONE)
         $this->setColor($color);
      fwrite($this->stream, $text);
   }

   public function writeTo($x, $y, $text, $style = self::STYLE_NONE, $color = self::COLOR_NONE)
   {
      $this->write(ANSI_CSI . $y . ';' . $x . 'H' . $text, $style, $color);
   }

   public function setCursor($x, $y)
   {
      $this->writeTo($x, $y, '');
   }

   public function setColor($color = self::COLOR_NONE)
   {
      $this->write(ANSI_CSI . (30 + $color) . 'm');
   }

   public function setStyle($style = self::STYLE_NONE)
   {
      if ($style == self::STYLE_NONE)
         $this->write(ANSI_CSI . 0 . 'm');
      else
      {
         if ($style & self::STYLE_BOLD)
            $this->write(ANSI_CSI . 1 . 'm');
         if ($style & self::STYLE_ITALIC)
            $this->write(ANSI_CSI . 3 . 'm');
         if ($style & self::STYLE_UNDERLINE)
            $this->write(ANSI_CSI . 4 . 'm');
         if ($style & self::STYLE_BLINK)
            $this->write(ANSI_CSI . 5 . 'm');
         if ($style & self::STYLE_INVERSE)
            $this->write(ANSI_CSI . 7 . 'm');
      }
   }

   public function clear()
   {
      $this->write(ANSI_CSI . 2 . 'J');
      $this->setCursor(null, null);
   }
}
