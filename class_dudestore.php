<?php

abstract class DudeStore
{
   static private $dude;

   static public function add(Dude $dude)
   {
      self::$dude = $dude;
   }

   static public function getDude()
   {
      return self::$dude;
   }
}
