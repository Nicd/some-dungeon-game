<?php

const DIR_NONE    = 0,
      DIR_UP      = 1,
      DIR_DOWN    = 2,
      DIR_RIGHT   = 4,
      DIR_LEFT    = 8;

const TERM_X = 126,
      TERM_Y = 40;
