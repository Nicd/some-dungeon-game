<?php

class Human extends AbstractBeing
{
   const CHAR = 'H';

   private $sex;

   public function __construct($name = null, $exp = 0, array $items = null, array $equips = null, $speed = null, $sight = null, $hp = 0, $mp = 0, $money = null, Place $place = null)
   {
      $this->sex = rand(0, 1);
      parent::__construct($name, $exp, $items, $equips, $speed, $sight, $hp, $mp, $money, $place);
   }

   public function setSex($sex)
   {
      $this->sex = $sex;
   }

   public function getSex()
   {
      if ($this->sex == 0)
         return 'male';
      return 'female';
   }
}
