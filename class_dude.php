<?php

class Dude implements Being
{
   private $base;

   public function __construct(Being $base)
   {
      $this->base = $base;
      DudeStore::add($this);
   }

   public function getBase()
   {
      return clone $this->base;
   }

   public function move($dir)
   {
      $this->base->move($dir);
   }

   public function __call($method, $arguments)
   {
      if (is_callable('parent::' . $method))
         call_user_func_array('parent::' . $method, $arguments);
      else
         throw new SDGException('Cannot call nonexistant method ' . $method);
   }
}
