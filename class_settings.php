<?php

class Settings
{
   private $settings;
   
   public function __construct()
   {
      $this->settings = array();
   }

   public function set($setting, $value)
   {
      $this->settings[$setting] = $value;
   }

   public function get($setting)
   {
      if (is_object($this->settings[$setting]))
         return clone $this->settings[$setting];
      else
         return $this->settings[$setting];
   }
}
