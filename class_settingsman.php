<?php

class SettingsMan
{
   static private $sets;

   static public function get($set = 0)
   {
      return self::$sets[$set];
   }

   static public function add($set = null, Settings $settings)
   {
      if ($set !== null)
         self::$sets[$set] = $settings;
      else
         self::$sets[] = $settings;
   }
}
