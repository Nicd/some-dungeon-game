<?php

class Coord
{
   private $x;
   private $y;

   static public function createFromString($str)
   {
      $str = explode(',', $str);
      return new self($str[0], $str[1]);
   }


   public function __construct($x, $y)
   {
      $this->x = $x;
      $this->y = $y;
   }

   public function getDistance(Coord $coord)
   {
      return round(sqrt(pow($coord->x - $this->x, 2) + pow($coord->y - $this->y, 2)));
   }

   public function __get($name)
   {
      if ($name == 'x' || $name == 'y')
      {
         return $this->$name;
      }
      else
         throw new SDGException('Can only access X and Y in ' . __CLASS__);
   }

   public function __toString()
   {
      return $this->x . ',' . $this->y;
   }
}
