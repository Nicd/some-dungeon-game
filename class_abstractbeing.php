<?php

abstract class AbstractBeing extends Entity implements Being
{
   const MAXITEMS = 5;
   const VISIBILITY = parent::VSB_VISIBLE;

   private $name;
   private $items;
   private $equips;
   private $speed;
   private $sight;
   private $hp;
   private $mp;
   private $money;
   private $exp;

   static public function createName(AbstractBeing $being)
   {
      $being->name = 'FAIL_' . uniqid();
   }

   static public function giveRandomItems(AbstractBeing $being)
   {
      /*$amount = rand(0, static::MAXITEMS);
      for ($i = 0; $i < $amount; ++$i)
      {
         $being->items[] = RandomItemFactory::getItem($being);
      }*/
   }

   static public function equipRandomItems(AbstractBeing $being)
   {
      /*$amount = rand(0, count($being->items));
      for ($i = 0; $i < $amount; ++$i)
      {
         try
         {
            $being->equip($being->items[$i]);
         } catch (CannotEquipException $e)
         { }
      }*/
   }

   static public function setRandomStats(AbstractBeing $being)
   {
      
   }



   public function __construct($name = null, $exp = 0, array $items = null, array $equips = null, $speed = null, $sight = null, $hp = 0, $mp = 0, $money = null, Place $place = null)
   {
      $this->name = $name;

      if ($items === null)
         static::giveRandomItems($this);

      if ($equips === null)
         static::equipRandomItems($this);

      static::setRandomStats($this);
      if ($speed !== null)
         $this->speed = $speed;
      if ($sight !== null)
         $this->sight = $sight;
      if ($hp > 0)
         $this->hp = $hp;
      if ($mp > 0)
         $this->mp = $mp;
      if ($money !== null)
         $this->money = $money;
      if ($exp > 0)
         $this->exp = $exp;
      
      if ($place != null)
         $this->setPlace($place);
   }

   public function setRandomPlace(Level $level = null)
   {
      $coord = $level->getEmptyPlace();
      $this->setPlace(new Place($level, $coord));
   }

   public function move($dir)
   {
      $newX = $this->place->getCoord()->x;
      $newY = $this->place->getCoord()->y;
      if ($dir & DIR_DOWN)
         ++$newY;
      elseif ($dir & DIR_UP)
         --$newY;
      if ($dir & DIR_RIGHT)
         ++$newX;
      elseif ($dir & DIR_LEFT)
         --$newX;
      
      $newCoord = new Coord($newX, $newY);
      if ($this->place->getLevel()->whatsAt($newCoord) instanceof EmptyFloor)
         $this->setPlace(new Place($this->place->getLevel(), $newCoord));
      
      // Clear sight data
      $this->seenCells = array();
   }

   public function getName()
   {
      return $this->name;
   }

   public function getItems()
   {
      return $this->items;
   }

   public function getEquips()
   {
      return $this->equips;
   }

   public function getSpeed()
   {
      return $this->speed;
   }

   public function getSight()
   {
      return $this->sight;
   }

   public function getHP()
   {
      return $this->hp;
   }

   public function getMP()
   {
      return $this->mp;
   }

   public function getMoney()
   {
      return $this->money;
   }

   public function getExp()
   {
      return $this->exp;
   }

   public function __toString()
   {
      if (DudeStore::getDude()->getBase() == $this)
         return '@';
      return parent::__toString();
   }
}
