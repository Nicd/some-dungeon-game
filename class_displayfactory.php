<?php

abstract class DisplayFactory
{
   static public function getDefault()
   {
      return new Display(TERM_X, TERM_Y, STDOUT);
   }

   static public function terminalSetRaw()
   {
      system('stty raw');
   }

   static public function terminalSetCooked()
   {
      system('stty cooked');
   }
}
