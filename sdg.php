<?php

// Some Dungeon Game
// 0.1
// © Nicd

require_once 'inc_stuffz.php';
require_once 'inc_ansi.php';

function __autoload($class)
{
   require_once 'class_' . strtolower($class) . '.php';
}

$disp = DisplayFactory::getDefault();
$disp->clear();
$disp->setColor(Display::COLOR_RED);

$dungeon = new Dungeon();

$level = new Level($dungeon);
$dungeon->addLevelBelow($level);

$human = new Human('', 0, null, null, null, 40);
$enemy = new Human();
$enemy->setRandomPlace($level);
$dude = new Dude($human);
$human->setRandomPlace($level);

$draw = new LevelDrawer($disp);

DisplayFactory::terminalSetRaw();

$disp->clear();
$draw->drawDudeView();

$input = '';
do
{
   $input = fread(STDIN, 1);
   if (ctype_digit($input))
   {
      $dude->move($input);
      $disp->clear();
      $draw->drawDudeView();
   }
   elseif ($input == 'd')
   {
      $enemy->drawPath($disp, $dude->getBase());
   }
} while($input != 'q');

$disp->setStyle();
DisplayFactory::terminalSetCooked();
